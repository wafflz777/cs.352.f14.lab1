package workqueue

// #cgo CFLAGS:  -I/data/software/all/include -I/opt/cctools/include
// #cgo LDFLAGS: -L/data/software/all/lib     -L/opt/cctools/lib      -ldttools -lm
// #include <cctools/debug.h>
// #include <cctools/work_queue.h>
import "C"

// Constants -------------------------------------------------------------------

// Work Queue Port values.
const (
	DEFAULT_PORT = C.WORK_QUEUE_DEFAULT_PORT // Use default Work Queue port
	RANDOM_PORT  = C.WORK_QUEUE_RANDOM_PORT  // Use random Work Queue port
)

// Work Queue Master modes.
const (
	STANDALONE = C.WORK_QUEUE_MASTER_MODE_STANDALONE // Don't use Catalog
	CATALOG    = C.WORK_QUEUE_MASTER_MODE_CATALOG    // Use Catalog
)

// Work Queue wait values.
const (
	WAIT_FOR_TASK = C.WORK_QUEUE_WAITFORTASK // Wait until a task is complete
)

// Work Queue file types.
const (
	INPUT    = C.WORK_QUEUE_INPUT   // Input file
	OUTPUT   = C.WORK_QUEUE_OUTPUT  // Output file
	CACHED   = C.WORK_QUEUE_CACHE   // Cache file
	UNCACHED = C.WORK_QUEUE_NOCACHE // Don't cache file
)

// Debug -----------------------------------------------------------------------

// SetDebugFlag activates the specified debug flag.
func SetDebugFlag(flag string) bool {
	return C.cctools_debug_flags_set(C.CString(flag)) == 1
}

// ClearDebugFlags resets all debug flags.
func ClearDebugFlags() {
	C.cctools_debug_flags_clear()
}

// Queues ----------------------------------------------------------------------

// Work Queue Master.
type WorkQueue struct {
	ptr *C.struct_work_queue
}

// NewWorkQueue creates a new Work Queue Master with the specified port.
func NewWorkQueue(port int) *WorkQueue {
	wq := &WorkQueue{}
	wq.ptr = C.work_queue_create(C.int(port))
	return wq
}

// SpecifyMode sets the Work Queue Master mode.
func (wq *WorkQueue) SpecifyMode(mode int) {
	C.work_queue_specify_master_mode(wq.ptr, C.int(mode))
}

// Name returns the Work Queue Master name.
func (wq *WorkQueue) Name() string {
	return C.GoString(C.work_queue_name(wq.ptr))
}

// SpecifyName sets the Work Queue Master name.
func (wq *WorkQueue) SpecifyName(name string) {
	C.work_queue_specify_name(wq.ptr, C.CString(name))
}

// Port returns the Work Queue Master port.
func (wq *WorkQueue) Port() int {
	return int(C.work_queue_port(wq.ptr))
}

// Submit queues the task for execution.
func (wq *WorkQueue) Submit(task *Task) int {
	return int(C.work_queue_submit(wq.ptr, task.ptr))
}

// Empty returns whether or not the Work Queue is empty.
func (wq *WorkQueue) Empty() bool {
	return C.work_queue_empty(wq.ptr) == 1
}

// Wait returns a completed task after the specified timeout.
func (wq *WorkQueue) Wait(timeout int) *Task {
	task := &Task{}
	task.ptr = C.work_queue_wait(wq.ptr, C.int(timeout))
	if task.ptr == nil {
		return nil
	} else {
		return task
	}
}

// Tasks -----------------------------------------------------------------------

// Work Queue Task.
type Task struct {
	ptr *C.struct_work_queue_task
}

// NewTask creates a WorkQueue task with the specified command.
func NewTask(command string) *Task {
	task := &Task{}
	task.ptr = C.work_queue_task_create(C.CString(command))
	return task
}

// Delete deallocates the memory used by the internal Task structure.  You
// should call this only when you are completely done with the Task.  Usually,
// the life cycle is as follows: create -> specify -> submit -> wait -> delete.
func (task *Task) Delete() {
	C.work_queue_task_delete(task.ptr)
}

// Command returns the Task's command line.
func (task *Task) Command() string {
	return C.GoString(task.ptr.command_line)
}

// Output returns the Task's captured output.
func (task *Task) Output() string {
	return C.GoString(task.ptr.output)
}

// ReturnStatus returns the Task's exit code.
func (task *Task) ReturnStatus() int {
	return int(task.ptr.return_status)
}

// ExitCode the Task's exit code.
func (task *Task) ExitCode() int {
	return int(task.ReturnStatus())
}

// SpecifyFile adds a file specification to the task.
func (task *Task) SpecifyFile(local_name string, remote_name string, file_type int, flags int) int {
	return int(C.work_queue_task_specify_file(
		task.ptr,
		C.CString(local_name),
		C.CString(remote_name),
		C.int(file_type),
		C.int(flags),
	))
}

// SpecifyInputFile adds an input file specification to the task.
func (task *Task) SpecifyInputFile(local_name string, remote_name string, flags int) int {
	return task.SpecifyFile(local_name, remote_name, INPUT, flags)
}

// SpecifyOutputFile adds an output file specification to the task.
func (task *Task) SpecifyOutputFile(local_name string, remote_name string, flags int) int {
	return task.SpecifyFile(local_name, remote_name, OUTPUT, flags)
}
