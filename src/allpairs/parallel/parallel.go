package parallel

import (
	"allpairs/metric"
	"allpairs/utils"
)

type ResultPair struct {
	Path string
	Hash uint64
}

// Perform All-Pairs comparison of imaages using goroutines.
func AllPairsParallel(paths []string, all bool) {
	var results = make(chan ResultPair)
	var hashes (map[string]uint64)
	hashes = make(map[string]uint64)

	for i:= range paths {
		path := paths[i]
		go func(path string) {
			results <- ResultPair{path, metric.ComputePerceptualHash(path)}
		}(path)
	}
	for i:=0; i < len(paths); i++ {
		var pair = <-results
		hashes[pair.Path] = pair.Hash
	}
	close(results)
	for pair := range utils.GenerateCombinations(paths, all) {
		distance := metric.ComputeHammingDistance(hashes[pair.First], hashes[pair.Second])
		metric.PrintHammingDistance(pair.First, pair.Second, distance)
	}
}
