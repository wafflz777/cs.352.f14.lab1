package metric

import (
	"fmt"
	"image/color"
	"log"
	"path"
	"sync"

	"github.com/disintegration/imaging"
	"github.com/harrydb/go/img/grayscale"
)

const (
	hashWidth  = 8
	hashHeight = 8
	hashPixels = hashWidth * hashHeight
)

// Compute the Perceptual Hash of an Image.
func ComputePerceptualHash(path string) uint64 {
	// Open image for processing
	var image,err = imaging.Open(path)
	if err != nil {
		log.Fatalf("error")
	}
	// Resize image to hashWidth x hashHeight using BSpline
	var resized = imaging.Resize(image, hashWidth, hashHeight, imaging.BSpline)
	// Reduce color to grayscale using ToGrayLuminance
	var grayscaled = grayscale.Convert(resized, grayscale.ToGrayLuminance)
	// Compute the average of the color values
	var total uint64 = 0
	for row:=0; row < hashHeight; row++ {
		for col:=0; col < hashWidth; col++ {
			var pixel = grayscaled.At(row, col).(color.Gray)
			total = total + uint64(pixel.Y)
		}
	}
	var average = float64(float64(total)/float64(hashPixels))
	// Compute the hash by setting the bits of a uint64 based on if color
	// value is above or below the mean.
	var hash uint64  = 0
	for row:=0; row < hashHeight; row++ {
		for col:=0; col < hashWidth; col++ {
			hash <<= 1
			var pixel = grayscaled.At(row, col).(color.Gray)
			if float64(pixel.Y) < average {
				hash |= 1
			}
		}
	}
	return hash
}
// Computer Perceptual Hashes for specified files
func ComputePerceptualHashesFromFiles(paths []string) {
	var wg sync.WaitGroup
	wg.Add(len(paths))
	for i := range paths {
		p := paths[i]
		go func(p string) {
			fmt.Printf("%s %v\n", path.Base(p), ComputePerceptualHash(p))
			defer wg.Done()
		}(p)
	}
	wg.Wait()
}
