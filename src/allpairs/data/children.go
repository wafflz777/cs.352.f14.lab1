package data

import (
	"os"
	"path"
)

var ChildrenImages = []string{
	"caleb0.jpg",
	"caleb1.jpg",
	"caleb2.jpg",
	"caleb3.jpg",
	"twins0.jpg",
	"twins1.jpg",
	"twins2.jpg",
	"twins3.jpg",
}

func GetChildrenPaths() []string {
	paths := []string{}

	for _, image := range ChildrenImages {
		paths = append(paths, path.Join(os.Getenv("GOPATH"), "data", image))
	}

	return paths
}
