package utils

// Pair of  values.
type Pair struct {
	First, Second string
}

// Generate combinations of pairs from given set of strings.
func GenerateCombinations(strings []string, all bool) <-chan Pair {
	ch := make(chan Pair)
	go func() {
		for i,first := range strings {
			rest := strings
			if all {} else {
				rest = strings[i:]
			}
			for _, second := range rest {
				p := Pair{first,second}
				ch <- p
			}
		}
		close(ch)
	}()
	return ch
}

const DefaultChunkSize = 8 // Default Chunk Size

// Generate chunks of fixed-size from input slice of strings.
func GenerateChunks(strings []string, size int) <-chan []string {
	var chunk = make([]string, 0)
	var cake int
	ch := make(chan []string)
	go func() {
		for i, varString := range strings {
			cake = i
			chunk = append(chunk, varString)
			if len(chunk) % size == 0 {
				ch <- chunk
				chunk = make([]string, 0)
			}
		}
		if len(chunk) > 0 {
			ch <- chunk
		}
		close(ch)
	}()
	return ch
}
