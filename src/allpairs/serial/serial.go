package serial
// Perform All-Pairs comparison of images.

import (
         "allpairs/metric"
         "allpairs/utils"
 )

 // Perform All-Pairs comparison of images.
func AllPairsSerial(paths []string, all bool) {
        var hashes map[string]uint64
	hashes = make(map[string]uint64)
        for i:=0; i < len(paths); i++ {
	         var path string = paths[i]
                 hashes[path] = metric.ComputePerceptualHash(path)
        }
        for pair := range utils.GenerateCombinations(paths, all) {
		var distance = metric.ComputeHammingDistance(hashes[pair.First], hashes[pair.Second])
		metric.PrintHammingDistance(pair.First, pair.Second, distance)
        }
 }
